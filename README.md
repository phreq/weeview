# WeeView
WeeView is a script for the **bourne-again shell (BASH)**. It allows You to search for text strings like dates, nicknames and keywords inside WeeChat's logs.
For now the script is very small but in the future I hope I'll add some new features.

![WeeView Banner](weeview.png)

Currently It has these features:

* search for **dates**, **nicknames** and **keywords**
* autocompletion
* error handling

I also added the binary executable, anyway, if You want to compile WeeView manually, just install ```shc``` and type ```shc -f weeview.sh```. It will create ```weeview.sh.x```, chmod +x It and rename It to ```weeview``` then put It inside ```/usr/bin``` (or your equivalent dir).
