#!/bin/bash

red='\E[31;47m'
green='\E[32;47m'
nocolor='\033[0m'
echo "
 ____ ____ ____ ____ ____ ____ ____
||W |||e |||e |||V |||i |||e |||w ||
||__|||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|/__\|"
echo
echo WeeChat\'s Logs Viewer - Script
echo
echo -e ${green}Enter WeeChat\'s Logs absolute path \(usually is /home/user/.weechat/logs\):${nocolor}
echo
read -e abspath
if [ ! -d "$abspath" ]; then
    echo -e ${red}Dir not found, exiting...${nocolor}
    exit
else
    echo
    echo -e ${green}Reading $abspath logs...${nocolor}
    cd $abspath
    sleep 1 && clear
    ls
    echo
    echo -e ${green}Enter the file to view:${nocolor}
    echo
    read -e logfile
    echo
    echo -e ${green}Enter the string to search for:${nocolor}
    echo
    read string
    echo
    echo -e ${green}Listing log file by specified string...${nocolor}
    sleep 1 && clear
    if [[ $(cat $logfile | grep --color $string) ]]; then
        cat $logfile | grep $string
        echo
        echo -e ${green}Done.${nocolor}
        echo
    else
        echo -e ${red}String not found.${nocolor}
        echo
    fi
fi

# Version 0.2
